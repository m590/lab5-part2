const mongoose = require('mongoose');
const Game = mongoose.model('Game');

module.exports.publisherGetOne = function (req, res) {
    const gameId = req.params.gameId;
    Game.findById(gameId).select("publisher").exec(function (err, game) {
        const response = {
            status: 200,
            content: game.publisher
        };

        if (err) {
            console.log('Error get game.publisher', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

const _addPublisher = function (req, res, game) {
    game.publisher.name = req.body.name;
    game.publisher.country = req.body.country;

    console.log('publisher', game.publisher);
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error creating publisher', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.publisherAddOne = function (req, res) {
    const gameId = req.params.gameId;

    Game.findById(gameId).exec(function (err, game) {
        console.log("game", game);
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _addPublisher(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

const _fullUpdatePublisher = function (req, res, game) {
    game.publisher.name = req.body.name;
    game.publisher.country = req.body.country;

    console.log('publisher', game.publisher);
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error saving publisher', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.publisherFullUpdateOne = function (req, res) {
    const gameId = req.params.gameId;

    Game.findById(gameId).exec(function (err, game) {
        console.log("game", game);
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _fullUpdatePublisher(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

const _partialUpdatePublisher = function (req, res, game) {
    if (req.body.name) {
        game.publisher.name = req.body.name;
    }
    if (req.body.country) {
        game.publisher.country = req.body.country;
    }

    console.log('publisher', game.publisher);
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error partially saving publisher', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.publisherPartialUpdateOne = function (req, res) {
    const gameId = req.params.gameId;

    Game.findById(gameId).exec(function (err, game) {
        console.log("game", game);
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _partialUpdatePublisher(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

const _deletePublisher = function (req, res, game) {
    game.publisher = {};

    console.log('publisher', game.publisher);
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            content: savedGame
        };

        if (err) {
            console.log('Error deleting publisher', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.publisherDeleteOne = function (req, res) {
    const gameId = req.params.gameId;

    Game.findById(gameId).exec(function (err, game) {
        console.log("game", game);
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            response.status = 500;
            response.content = err;
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 201) {
            _deletePublisher(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}