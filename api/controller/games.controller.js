const mongoose = require('mongoose');
const Game = mongoose.model("Game");

module.exports.gamesGetAll = function (req, res) {
    let count = 1;
    if (req.query && req.query.count) {
        count = parseInt(req.query.count);
    }
    let offset = 0;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }
    if (isNaN(count) || isNaN(offset)) {
        res.status(400).json({content: 'Query parameter count and offset should be numbers.'});
        return;
    }

    Game.find().skip(offset).limit(count).exec(function (err, games) {
        const response = {
            status: 200,
            content: games
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else {
            res.status(response.status).json(response.content);
        }
    });

}

module.exports.gamesGetOne = function (req, res) {
    const gameId = req.params.gameId;

    Game.findById(gameId).exec(function (err, game) {
        const response = {
            status: 200,
            content: game
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!game) {
            response.status = 400;
            response.content = {content: 'Game not found'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.gamesAddOne = function (req, res) {
    console.log("Add One games");

    const newGame = {
        title: req.body.title,
        year: parseInt(req.body.year),
        price: parseFloat(req.body.price),
        minPlayers: parseInt(req.body.minPlayers),
        maxPlayers: parseInt(req.body.maxPlayers),
        minAge: parseInt(req.body.minAge),
        designers: [],
        publisher: {}
    };
    if (req.body.rate) {
        newGame.rate = parseInt(req.body.rate);
    }
    console.log('newGame', newGame);

    Game.create(newGame, function (err, game) {
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            console.log("Error creating game", err);
            response.status = 500;
            response.message = "Internal error";
        }
        console.log('Game saved', game);
        res.status(response.status).json(response.content);
    });
}

const _fullUpdateGame = function (req, res, game) {
    game.title = req.body.title;
    game.year = parseInt(req.body.year);
    game.rate = parseInt(req.body.rate);
    game.price = parseFloat(req.body.price);
    game.minPlayers = parseInt(req.body.minPlayers);
    game.maxPlayers = parseInt(req.body.maxPlayers);
    game.minAge = parseInt(req.body.minAge);

    game.save(function (err, game) {
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            console.log("Error creating game", err);
            response.status = 500;
            response.content = "Internal error";
        }
        console.log('Game full updated', game);
        res.status(response.status).json(response.content);
    });
}

module.exports.gamesFullUpdateOne = function (req, res) {
    console.log("Full update one game");

    const gameId = req.params.gameId;
    Game.findById(gameId).exec(function (err, game) {
        const response = {
            status: 200,
            content: game
        };

        if (err) {
            response.status = 500;
            response.content = 'Internal error';
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 200) {
            // found game
            _fullUpdateGame(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }

    });

}

const _partialUpdateGame = function (req, res, game) {
    if (req.body.title) {
        game.title = req.body.title;
    }
    if (req.body.year) {
        game.year = parseInt(req.body.year);
    }
    if (req.body.rate) {
        game.rate = parseInt(req.body.rate);
    }
    if (req.body.price) {
        game.price = parseFloat(req.body.price);
    }
    if (req.body.minPlayers) {
        game.minPlayers = parseInt(req.body.minPlayers);
    }
    if (req.body.maxPlayers) {
        game.maxPlayers = parseInt(req.body.maxPlayers);
    }
    if (req.body.minAge) {
        game.minAge = parseInt(req.body.minAge);
    }

    game.save(function (err, game) {
        const response = {
            status: 201,
            content: game
        };
        if (err) {
            console.log("Error creating game", err);
            response.status = 500;
            response.content = "Internal error";
        }
        console.log('Game partially updated', game);
        res.status(response.status).json(response.content);
    });
}

module.exports.gamesPartialUpdateOne = function (req, res) {
    console.log("Partial update one game");

    const gameId = req.params.gameId;
    Game.findById(gameId).exec(function (err, game) {
        const response = {
            status: 200,
            content: game
        };

        if (err) {
            response.status = 500;
            response.content = 'Internal error';
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        if (response.status === 200) {
            // found game
            _partialUpdateGame(req, res, game);
        } else {
            res.status(response.status).json(response.content);
        }
    });
}

module.exports.gamesDeleteOne = function (req, res) {
    console.log("Delete one game");

    const gameId = req.params.gameId;
    Game.findByIdAndDelete(gameId).exec(function (err, game) {
        const response = {
            status: 204
        };

        if (err) {
            response.status = 500;
            response.content = 'Internal error';
        } else if (!game) {
            response.status = 404;
            response.content = 'Game not found';
        }
        res.status(response.status).json(response.content);
    });
}